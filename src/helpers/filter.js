export const filteredByYears = (consoArray, search) => {
    let FilteredDataArray = consoArray.filter(
        d => {
            return d.createdAt.indexOf(search) !== -1;
        }
    ); return FilteredDataArray;
};