export const dateToFRFormat = date => {
    const options = { 
        /*weekday: 'long', 
        year: 'numeric', 
        month: 'long', 
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric'*/
    };
    let formatedDate = new Date(date).toLocaleDateString('fr-FR' , options);
    return formatedDate;
};