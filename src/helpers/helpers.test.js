import { dateToFRFormat } from './dateFormat';
import { filteredByYears }  from './filter';

describe('helpers', () => {

    it('should be formated like', () => {
        const date = new Date('2019-04-20T12:50:32.038Z');
        expect(dateToFRFormat(date)).toBe('2019-4-20');
    });

    it('should give an array with 2019', () => {
        
        const array = [
            { id: 1, createdAt: '2015'},
            { id: 2, createdAt: '2019'},
            { id: 3, createdAt: '2016'},
            { id: 4, createdAt: '2019'},
        ]
        const search = '2019';
        let filteredList = filteredByYears(array, search);
        expect(filteredList).toContain([
            { id: 2, createdAt: '2019' },
            { id: 4, createdAt: '2019' },
        ]);
    });
});