import React from 'react';
import './App.scss';
import { ConsoState } from '../contexts/consoContext/ConsoContext';
import ConsoPage from '../pages/consoPage/ConsoPage';
import UIState from '../contexts/UIContext/UIContext';
import { SearchState } from '../contexts/searchContext/SearchContext';



function App() {
  return (
    <SearchState>
    <UIState>
    <ConsoState>
      <div className="App">
        <ConsoPage/>
      </div>
    </ConsoState>
    </UIState>
    </SearchState>
  );
}

export default App;
