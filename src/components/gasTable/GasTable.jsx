import React, { useContext } from 'react';
import { ConsoContext } from '../../contexts/consoContext/ConsoContext';
import { dateToFRFormat } from '../../helpers/dateFormat';
import { SearchContext } from '../../contexts/searchContext/SearchContext';
import { filteredByYears } from '../../helpers/filter';

const GasTable = () => {
    const consoContext = useContext(ConsoContext);
    const gasData = consoContext.gasData;
    const [search] = useContext(SearchContext);
    let filteredData = filteredByYears(gasData, search);

    return ( 
        <table>
            <thead>
                <tr>
                    <th>date</th>
                    <th>consommation</th>
                </tr>
            </thead>
            <tbody> 
                {filteredData.map(gas =>
                    <tr key={gas.id}>
                        <td>{dateToFRFormat(gas.createdAt)}</td>
                        <td>{gas.indexHigh} m3</td>
                    </tr>
                )}
            </tbody>
        </table>
     );
}
 
export default GasTable;