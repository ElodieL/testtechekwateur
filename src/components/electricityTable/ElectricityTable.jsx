import React, { useContext } from 'react';
import { ConsoContext } from '../../contexts/consoContext/ConsoContext';
import { dateToFRFormat } from '../../helpers/dateFormat';
import { SearchContext } from '../../contexts/searchContext/SearchContext';
import { filteredByYears } from '../../helpers/filter';

const ElectricityTable = () => {
    const consoContext = useContext(ConsoContext);
    const electricityData = consoContext.electricityData;
    const [search] = useContext(SearchContext);
    let filteredData = filteredByYears(electricityData, search);

    return ( 
        <table>
            <thead>
                <tr>
                    <th>date</th>
                    <th>heures pleines</th>
                    <th>heures creuses</th>
                </tr>
            </thead>
            <tbody> 
                {filteredData.map(electricity =>
                    <tr key={electricity.id}>
                        <td>{dateToFRFormat(electricity.createdAt)}</td>
                        <td>{electricity.indexhigh} kWh</td>
                        <td>{electricity.indexLow} kWh</td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}
 
export default ElectricityTable;