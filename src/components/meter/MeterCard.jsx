import React, { useContext } from 'react';
import './MeterCard.scss';
import { ConsoContext } from '../../contexts/consoContext/ConsoContext';
import electricityIcon from '../../assets/electricity.png';
import gasIcon from '../../assets/gas.png';
import { UIContext } from '../../contexts/UIContext/UIContext';
import { dateToFRFormat } from '../../helpers/dateFormat';

const MeterCard = () => {
    const consoContext = useContext(ConsoContext);
    const ui_context = useContext(UIContext)
    const meterData = consoContext.meterData;
    const consoSelected = ui_context.consoType; // gas: '1' / electricity '2'

    return ( 
        <div className="MeterCards">
            {meterData.map(meter =>
                <div className={meter.id === consoSelected ? 'selected' : 'MeterCards__card'} 
                    key={meter.id} 
                    onClick={() => ui_context.showConsoType(meter.id)}
                >
                    <img className="MeterCards__picture" 
                        src={meter.id  === '1' ? gasIcon : electricityIcon} 
                        alt={meter.id  === '1' ? "gaz" : "électricité"}
                    />
                    <h2 className="MeterCards__title">{meter.id  === '1' ? 'GAZ' : 'ÉLECTRICITÉ'}</h2>
                    <p>Compteur n° : {meter.pointOfDelivery} </p>
                    <p>Ouverture : {dateToFRFormat(meter.createdAt)}</p>
                </div>
            )}
        </div>
    );
}
 
export default MeterCard;