import React, { useContext } from 'react';
import './SearchBar.scss';
import { SearchContext } from '../../contexts/searchContext/SearchContext';

const SearchBar = () => {
    const [search, setSearch] = useContext(SearchContext);
    const updateSearchEvent = event => {
        setSearch(event);
        return console.log(search)
    }
    return ( 
        <div className="SearchBar">
            <input   
                type="text" 
                placeholder="Rechercher une année"
                value={search} 
                onChange={event => updateSearchEvent(event.target.value)}
            />
        </div>
     );
}
 
export default SearchBar;