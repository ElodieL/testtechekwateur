import React, { useContext } from 'react';
import './ConsoPage.scss';
import MeterCard from '../../components/meter/MeterCard';
import GasTable from '../../components/gasTable/GasTable';
import ElectricityTable from '../../components/electricityTable/ElectricityTable';
import { UIContext } from '../../contexts/UIContext/UIContext';
import SearchBar from '../../components/searchBar/SearchBar';

const ConsoPage = () => {
    const ui_context = useContext(UIContext);
    let consoType = ui_context.consoType;
    const GAS = '1';
    const ELECTRICITY = '2';

    return ( 
        <div className="ConsoPage">
            <div className ="ConsoPage__header">
                <h1>Ma consommation d'énergie</h1>
                <SearchBar/>
                <MeterCard/>
            </div>
            
            <div className="ConsoPage__content">
                { consoType === '' && ''}
                { consoType === GAS && <GasTable/> } 
                { consoType === ELECTRICITY && <ElectricityTable/> }
            </div>
        </div>
     );
}
 
export default ConsoPage;