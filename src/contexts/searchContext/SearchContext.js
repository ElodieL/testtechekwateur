import React, { useState, createContext } from 'react';

export const SearchContext = createContext();

export const SearchState = props => {
    //state for searchbar
    const [search, setSearch] = useState('');

    return ( 
        <SearchContext.Provider value = {[search, setSearch]}>
            {props.children}
        </SearchContext.Provider>
    );
}
 