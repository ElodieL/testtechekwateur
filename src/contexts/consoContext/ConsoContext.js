import React, { createContext, useState, useEffect } from 'react';

export const ConsoContext = createContext({});

export const ConsoState = props => {
    const [meterData, setMeterData] = useState([]);
    const [electricityData, setElectricityData] = useState([]);
    const [gasData, setGasData] = useState([]);

    useEffect(() => {
        const fetchMeterData = async () => {
            const response = await fetch('https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/');
            const jsonResponse = await response.json();
            //console.log(jsonResponse);
            setMeterData(jsonResponse);
        };

        const fetchGasData = async () => {
            const response = await fetch('https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/1/gas');
            const jsonResponse = await response.json();
            //console.log(jsonResponse);
            setGasData(jsonResponse);
        };

        const fetchElectricityData = async () => {
            const response = await fetch('https://5e9ed3cdfb467500166c47bb.mockapi.io/api/v1/meter/2/electricity');
            const jsonResponse = await response.json();
            //console.log(jsonResponse);
            setElectricityData(jsonResponse);
        };
        fetchMeterData();
        fetchElectricityData();
        fetchGasData();
    }, []);

    return ( 
        <ConsoContext.Provider
            value = {{
                meterData: meterData,
                electricityData: electricityData,
                gasData: gasData
            }}
        >
            {props.children}
        </ConsoContext.Provider>
    );
};
 