export const SHOW_CONSO_TYPE = 'SHOW_CONSO_TYPE';

const showConso = (consoType, state) => {
    const updatedConsoType = consoType;
    return {...state, consoType: updatedConsoType};
};

export const UIReducer = (state, action) => {
    switch(action.type) {
        case SHOW_CONSO_TYPE:
            return showConso(action.consoType, state);
        default:
            return state; 
    };
};