import React, { createContext, useReducer } from 'react';
import { UIReducer, SHOW_CONSO_TYPE } from './UIReducer';

export const UIContext = createContext({});

export const UIState = props => {
    const [consoTypeState, dispatch] = useReducer(UIReducer, {consoType: ''});

    //To display gas or electricity table (consoType '1' = gas, '2' electricity)
    const showConsoType = consoType => {
        dispatch({type: SHOW_CONSO_TYPE, consoType : consoType});
    }

    return ( 
        <UIContext.Provider
            value = {{
                consoType: consoTypeState.consoType,
                showConsoType: showConsoType,
            }}
        >
            {props.children}
        </UIContext.Provider>

    );
}
 
export default UIState;